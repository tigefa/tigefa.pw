const { htmlTemplate, footer } = require('./helpers');

exports.helpContent = `

/$$   /$$ /$$$$$$$$ /$$       /$$$$$$$
| $$  | $$| $$_____/| $$      | $$____$$
| $$  | $$| $$      | $$      | $$    $$
| $$$$$$$$| $$$$$   | $$      | $$$$$$$/
| $$__  $$| $$__/   | $$      | $$____/
| $$  | $$| $$      | $$      | $$
| $$  | $$| $$$$$$$$| $$$$$$$$| $$
|__/  |__/|________/|________/|__/

---------------------------------------------------------------------------------

# Source 1 stats - updated once a day from John Hopkins University
https://tigefa.pw

---------------------------------------------------------------------------------

(DEFAULT SOURCE)
# Source 2 stats - updated every 15 minutes from worldometers.info
https://tigefa.pw?source=2

---------------------------------------------------------------------------------

# Country wise stats

## Format:
https://tigefa.pw/[countryCode]
https://tigefa.pw/[countryName]

## Example: From source 1
https://tigefa.pw/Italy?source=1
https://tigefa.pw/UK?source=1

## Example: From source 2 (DEFAULT)
https://tigefa.pw/italy
https://tigefa.pw/italy?source=2
https://tigefa.pw/UK?source=2
https://tigefa.pw/UK

---------------------------------------------------------------------------------

# State wise api (Only for US as of now)

## Format:
https://tigefa.pw/states/[countryCode]
https://tigefa.pw/states/[countryName]

## Example: From source 1
https://tigefa.pw/us
https://tigefa.pw/USA?format=json
https://tigefa.pw/USA?minimal=true

---------------------------------------------------------------------------------

# Minimal Mode - remove the borders and padding from table

## Example:
https://tigefa.pw?minimal=true
https://tigefa.pw/Italy?minimal=true           (with country filter)
https://tigefa.pw?minimal=true&source=1        (with source)
https://tigefa.pw/uk?source=2&minimal=true     (with source and country)

---------------------------------------------------------------------------------

# Get data as JSON - Add ?format=json

## Example:
https://tigefa.pw?format=json
https://tigefa.pw/Italy?format=json            (with country filter)
https://tigefa.pw/?source=2&format=json        (with source)
https://tigefa.pw/uk?source=2&format=json      (with source and country)

---------------------------------------------------------------------------------

# Get top N countries - Add ?top=N

## Example:
https://tigefa.pw?top=25
https://tigefa.pw?source=1&top=10               (with source)
https://tigefa.pw/uk?minimal=true&top=20        (with minimal)


---------------------------------------------------------------------------------

# Confirmed Cases Graph (WIP)

## Format:
https://tigefa.pw/[countryName]/graph
https://tigefa.pw/[countryCode]/graph

## Example:
https://tigefa.pw/italy/graph
https://tigefa.pw/china/graph


------------- Any issues or feedback - Hit me up on twitter @ekrysis --------------

`;

exports.countryNotFound = (isCurl) =>  {
  const body = `
    Country not found.
    Try the full country name or country code.
    Example:
      - /UK: for United Kingdom
      - /US: for United States of America.
      - /Italy: for Italy.

    ${footer(new Date)}
  `;
  return isCurl ? body : htmlTemplate(body);
};

exports.stateCountryNotFound = (isCurl) => {
  const body = `
    State wise api is only available for:
      - US
    Try:
    /US or /USA

    ${footer(new Date)}
  `;
  return isCurl ? body : htmlTemplate(body);
};
